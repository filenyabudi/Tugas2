package com.example.sukangoding.simpleintens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnTgs1, btnTgs2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTgs1 = (Button)findViewById(R.id.btnTugas1);
        btnTgs2 = (Button)findViewById(R.id.btnTugas2);

        btnTgs1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, subTugas1.class);
                startActivity(intent);
            }
        });

        btnTgs2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, subTugas2.class);
                startActivity(intent);
            }
        });


    }
}
